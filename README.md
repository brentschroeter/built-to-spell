# Built to Spell

## What It Is

A browser-based game inspired by the New York Times Spelling Bee. Also, a
weekend project to expand my CSS3 skill set (and my vocabulary).

## What It Isn't

An exact clone of the NYT Spelling Bee. It uses a different word list (TWL06,
versus the bespoke lists generated each day by the Times). Don't go bragging to
your friends when you beat their scores; they might be playing by different
rules. (If you're playing Built to Spell, of course, you're still automatically
the coolest kid on the block regardless.)

## Play It

[https://bts.brentschroeter.com/](https://bts.brentschroeter.com/)
