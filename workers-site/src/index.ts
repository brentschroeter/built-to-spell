import {
  getAssetFromKV,
  Options as GetAssetFromKVOptions,
} from '@cloudflare/kv-asset-handler';

/**
 * The DEBUG flag will do two things that help during development:
 * 1. we will skip caching on the edge, which makes it easier to
 *    debug.
 * 2. we will return an error message on exception in your Response rather
 *    than the default 404.html page.
 */
const DEBUG = false;

addEventListener('fetch', (event) => {
  try {
    event.respondWith(handleFetch(event));
  } catch (e) {
    if (e instanceof Error && DEBUG) {
      return event.respondWith(new Response(e.message || e.toString(), { status: 500 }));
    }
    event.respondWith(new Response('Internal Error', { status: 500 }));
  }
});

addEventListener('scheduled', (event) => {
  event.waitUntil(handleCron());
});

interface DailyGameData {
  printDate: string;
  centerLetter: string;
  outerLetters: string[];
  answers: string[];
}

interface GameData {
  today: DailyGameData;
}

async function handleFetch(event: FetchEvent) {
  const url = new URL(event.request.url);
  const options: Partial<GetAssetFromKVOptions> = {};
  try {
    if (DEBUG) {
      options.cacheControl = { bypassCache: true };
    }
    let response = new Response('', {});
    if (url.pathname.toLocaleLowerCase('en') === '/api/game-data/latest') {
      const gameData = await NYT_GAME_DATA.get('latest');
      response = new Response(gameData || '', { status: gameData ? 200 : 500 });
      response.headers.set('Content-Type', 'application/json');
    } else {
      const page = await getAssetFromKV(event, options);
      response = new Response(page.body, page);
    }
    response.headers.set('X-XSS-Protection', '1; mode=block');
    response.headers.set('X-Content-Type-Options', 'nosniff');
    response.headers.set('X-Frame-Options', 'DENY');
    response.headers.set('Referrer-Policy', 'unsafe-url');
    response.headers.set('Feature-Policy', 'none');
    return response;
  } catch (e) {
    // if an error is thrown try to serve the asset at 404.html
    if (e instanceof Error && DEBUG) {
      return new Response(e.message || e.toString(), { status: 500 });
    }
    try {
      let notFoundResponse = await getAssetFromKV(event, {
        mapRequestToAsset: (req) => new Request(`${new URL(req.url).origin}/404.html`, req),
      });
      return new Response(notFoundResponse.body, { ...notFoundResponse, status: 404 });
    } catch (e) { /* no-op */ }
    return new Response('Not found.', { status: 404 });
  }
}

async function handleCron() {
  console.log('Starting cron trigger...');
  const resp = await fetch('https://www.nytimes.com/puzzles/spelling-bee');
  console.log('Fetched from NYT.');
  let text = await resp.text();
  const startStr = '<script type="text/javascript">window.gameData = {';
  const startIdx = text.indexOf(startStr);
  if (startIdx === -1) {
    throw new Error('Unable to locate gameData.');
  }
  text = text.slice(startIdx + startStr.length - 1);
  const endIdx = text.indexOf('</script>');
  if (endIdx === -1) {
    throw new Error('Unable to locate end of gameData.');
  }
  text = text.slice(0, endIdx);
  console.log('Found gameData.');
  const gameData = JSON.parse(text) as GameData;
  console.log('Parsed gameData.');
  await Promise.all([
    NYT_GAME_DATA.put('latest', text),
    NYT_GAME_DATA.put(gameData.today.printDate, text),
  ]);
  console.log('Saved gameData.');
}
